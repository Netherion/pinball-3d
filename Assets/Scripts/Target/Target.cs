﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public bool canGoDown = false;
    public bool canGoUp = false;
    public bool isDown;

    GameObject controller;
    Controller contr;

    [SerializeField] float speed = 0;
    [SerializeField] int puntos = 10;

    private void Awake()
    {
        controller = GameObject.FindGameObjectWithTag("GameController");
        contr = controller.GetComponent<Controller>();
    }
    private void Update()
    {
        if (canGoDown)
        {
            transform.Translate(new Vector3(0, -speed, 0) * Time.deltaTime, Space.World);
            if (transform.position.y <= -2)
            {
                canGoDown = false;
                isDown = true;
            }
        }

        if (canGoUp)
        {
            transform.Translate(new Vector3(0, speed, 0) * Time.deltaTime, Space.World);
            if (transform.position.y >= 0)
            {
                canGoUp = false;
                isDown = false;
            }
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        canGoDown = true;
        contr.addPuntos(puntos);
        GetComponent<AudioSource>().Play();
    }
}
