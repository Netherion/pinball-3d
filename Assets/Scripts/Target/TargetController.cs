﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetController : MonoBehaviour
{
    [SerializeField] GameObject target1;
    [SerializeField] GameObject target2;
    [SerializeField] GameObject target3;
    [SerializeField] int puntos = 30;

    GameObject controller;
    Controller contr;
    private void Awake()
    {
        controller = GameObject.FindGameObjectWithTag("GameController");
        contr = controller.GetComponent<Controller>();
    }

    bool down1 = false, down2 = false, down3 = false;
    private void Update()
    {
        down1 = target1.GetComponent<Target>().isDown;
        down2 = target2.GetComponent<Target>().isDown;
        down3 = target3.GetComponent<Target>().isDown;

        if (down1 && down2 && down3)
        {
            target1.GetComponent<Target>().canGoUp = true;
            target2.GetComponent<Target>().canGoUp = true;
            target3.GetComponent<Target>().canGoUp = true;
            
            if(!contr.gameover)
            {
                contr.addPuntos(puntos);
                GetComponent<AudioSource>().Play();
            }
            
        }

        if (Input.GetKey(KeyCode.Q))
        {
            target1.GetComponent<Target>().canGoDown = true;
        }
        if (Input.GetKey(KeyCode.W))
        {
            target2.GetComponent<Target>().canGoDown = true;
        }
        if (Input.GetKey(KeyCode.E))
        {
            target3.GetComponent<Target>().canGoDown = true;
        }
    }

    public void resetTargets()
    {
        if (!down1)
        {
            target1.GetComponent<Target>().canGoDown = true;
        }

        if (!down2)
        {
            target2.GetComponent<Target>().canGoDown = true;
        }

        if (!down3)
        {
            target3.GetComponent<Target>().canGoDown = true;
        }
    }
}
