﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bouncer : MonoBehaviour
{
    [SerializeField] int fuerza;
    [SerializeField] int puntos = 10;
    private Rigidbody ballRb;
    Controller controller;
    void Start()
    {
        ballRb = GetComponent<Rigidbody>();
        controller = GameObject.FindGameObjectWithTag("GameController").GetComponent<Controller>();
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Bumper")
        {
            Vector3 normalVector = new Vector3(other.contacts[0].normal.x, 0, other.contacts[0].normal.z);

            ballRb.AddForce(normalVector * fuerza, ForceMode.Acceleration);

            controller.addPuntos(puntos);

            other.gameObject.GetComponent<AudioSource>().Play();
        }
    }
}
