﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{
    public GameObject ball;
    public GameObject puertecita;
    public Text score;
    public Text lives;
    public Text gameOverText;
    public bool gameover = false;
    public int vidasActuales = 3;

    public AudioSource coin;

    TargetController tc;

    private void Awake()
    {
        tc = GameObject.FindGameObjectWithTag("TargetController").GetComponent<TargetController>();
    }

    public void addPuntos(int puntos)
    {
        if (!gameover)
        {
            int puntosAct = int.Parse(score.text);
            score.text = (puntosAct + puntos).ToString();                        
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        vidasActuales--;
        GetComponent<AudioSource>().Play();
        Invoke("resetAll", 1);
    }

    public void resetAll()
    {
        if (vidasActuales != 0)
        {
            ball.transform.position = new Vector3(18.67f, 0.5f, 10.59f);
            ball.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
            puertecita.transform.position = new Vector3(16.445f, -2f, 47.253f);
        }
        else
        {
            gameover = true; 
            gameOverText.gameObject.SetActive(true);
            tc.resetTargets();           
        }

        lives.text = vidasActuales.ToString();
    }

    public void restartGame()
    {
        if (gameover)
        {
            score.text = "0";
            vidasActuales = 3;
            lives.text = vidasActuales.ToString();
            gameover = false;
            resetAll();     
            gameOverText.gameObject.SetActive(false);
            coin.Play();
        }
    }
}
