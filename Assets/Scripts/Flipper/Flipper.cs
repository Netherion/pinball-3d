﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flipper : MonoBehaviour
{

    public float restPosition = 0f;
    public float pressedPosition = 45f;
    public float hitStrenght = 10000f;
    public float flipperDamper = 150f;

    HingeJoint hingle;

    AudioSource src;
    public string inputName;

    void Start()
    {
        hingle = GetComponent<HingeJoint>();
        hingle.useSpring = true;
        src = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        JointSpring spring = new JointSpring();
        spring.spring = hitStrenght;
        spring.damper = flipperDamper;

        if (Input.GetAxis(inputName) == 1)
        {
            spring.targetPosition = pressedPosition;
            if(!src.isPlaying)
            {
                src.Play();
            }            
        }
        else
        {
            spring.targetPosition = restPosition;

        }
        hingle.spring = spring;
        hingle.useLimits = true;

    }
}
