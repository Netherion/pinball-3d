﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puertecita : MonoBehaviour
{
    [SerializeField] GameObject puerta;
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag=="Player")
        {
            puerta.transform.position = new Vector3(16.445f,0,47.253f);
        }
    }
}
